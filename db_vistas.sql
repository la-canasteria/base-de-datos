
-- --------------vista para filtrar 1---------------------

DROP VIEW IF EXISTS v_descuento_fitro1;
CREATE VIEW v_descuento_fitro1 AS
SELECT 
	dct.ntraDescuento, 
	dct.descripcion, 
	dct.fechaInicial, 
	dct.fechaFin, 
	dct.horaInicial, 
	dct.horaFin, 
	dct.estado, 
	(select descripcion from tblconcepto where codConcepto = 3 and correlativo = dct.estado) AS descEstado,
	dct.tipoDescuento,
	(select descripcion from tblconcepto where codConcepto = 6 and correlativo = dct.tipoDescuento) as descTipoDescuento,
	(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 1) as marca,
	(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 2 LIMIT 1) as tipoTarjeta,
	(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 3) as porcentaje,
	(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 4) as banco,
	(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 5) as categoria
FROM tbldescuentos dct
WHERE dct.marcaBaja = 0
;
/*
SELECT dct.ntraDescuento, dct.descripcion, dct.fechaInicial, dct.fechaFin, dct.horaInicial, dct.horaFin, dct.estado, dct.tipoDescuento,
(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 1) as marca,
(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 2 LIMIT 1) as tipo,
(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 3) as porcentaje,
(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 4) as banco,
(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 5) as categoria
FROM tbldescuentos dct
WHERE dct.marcaBaja = 0
;
*/


-- --------------vista para filtrar 2---------------------

DROP VIEW IF EXISTS v_descuento_fitro2;
CREATE VIEW v_descuento_fitro2 AS
SELECT 
	vi.ntraDescuento, 
	vi.descripcion, 
	vi.fechaInicial, 
	vi.fechaFin, 
	vi.horaInicial, 
	vi.horaFin, 
	vi.porcentaje,
	vi.tipoDescuento,
	vi.estado AS codEstado, 
	(SELECT descripcion FROM tblconcepto WHERE codConcepto = 3 AND vi.estado = correlativo) AS desEstado, 
	vi.marca AS codMarca, 
	(SELECT descripcion FROM tblconcepto WHERE codConcepto = 1 AND vi.marca = correlativo) AS desMarca, 
	vi.tipoTarjeta AS codTipo, 
	(SELECT descripcion FROM tblconcepto WHERE codConcepto = 2 AND vi.tipoTarjeta = correlativo) AS desTipo, 
	ban.codBanco AS codBanco, 
	ban.abreviatura AS abrBanco, 
	ban.descripcion AS desBanco,
	cat.codCategoria AS codCategoria, 
	cat.descCategoria AS descCategoria
FROM v_descuento_fitro1 vi
INNER JOIN tblbanco ban ON vi.banco = ban.codBanco
INNER JOIN tblCategoria cat ON cat.codCategoria = vi.categoria
;


-- --------------vista para filtrar datos del codigo BIN---------------------

DROP VIEW IF EXISTS v_bin_filtros;
CREATE VIEW v_bin_filtros AS
SELECT 
	`bin`.`ntraParametrizador` AS `ntraParametrizador`,
	`bin`.`codBIN` AS `codBIN`,
	`bin`.`marca` AS `idMarca`,
	(select descripcion from tblconcepto where codConcepto = 1 and correlativo = bin.marca) AS `descMarca`,
	`bin`.`tarjeta` AS `codTarjeta`,
	(select descripcion from tblconcepto where codConcepto = 2 and correlativo = bin.tarjeta) AS `descTarjeta`,
	`bin`.`banco` AS `codBanco`,
	`ban`.`descripcion` AS `descBanco`,
	`bin`.`marcaBaja` AS `marcaBaja` 
from tblcodbin as bin join tblbanco as ban on bin.banco = ban.codBanco
where bin.marcaBaja = 0
and ban.marcaBaja = 0
;


-- -------------- VISTA LISTAR DESCUENTO ESPECIAL  ---------------------

/*
DROP VIEW IF EXISTS v_descuento_especial;
CREATE VIEW v_descuento_especial AS
SELECT dct.ntraDescuento, dct.descripcion, dct.fechaInicial, dct.fechaFin, dct.horaInicial, dct.horaFin, 
dct.estado,
(select descripcion from tblconcepto where codConcepto = 3 and correlativo = dct.estado) AS descEstado,
dct.tipoDescuento,
(select descripcion from tblconcepto where codConcepto = 6 and correlativo = dct.tipoDescuento) as descTipoDescuento,
(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 2 LIMIT 1) as tipoTarjeta,
(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 4) as banco,
(select dt.valorInicial from tbldetalledescuentos dt where dt.ntraDescuento = dct.ntraDescuento and dt.flag = 5) as categoria
FROM tbldescuentos dct
WHERE dct.marcaBaja = 0
AND tipoDescuento = 2
;
*/



DROP VIEW IF EXISTS v_reporte_Ventas;
CREATE VIEW v_reporte_Ventas AS
SELECT 
	t.codvent, 
	t.fechped, 
	d.desarti,
	d.codcate,
	(SELECT descCategoria FROM tblcategoria WHERE codCategoria = d.codcate) AS categoria,
	t.codbanc AS banco,
	(SELECT descripcion FROM tblbanco bc WHERE bc.codbanco = t.codbanc AND bc.marcabaja= 0) AS descbanco,
	t.tiptarj AS tarjeta,
	d.cantive,
	d.importe,
	(SELECT importe FROM tbldescuentoventa dv WHERE dv.codvent = t.codvent AND codarti = d.codarti AND d.numitem = dv.numitem AND marbaja= 0) AS impdscto,
	t.impvent
FROM tblventas t, tbldetventa d 
WHERE t.codvent = d.codvent
AND t.marBaja = 0
AND d.marBaja = 0;
