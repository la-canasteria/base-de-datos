-- --------------Listar	Conceptos---------------------
DROP PROCEDURE IF EXISTS pa_listar_datos_select_x_flag;
DELIMITER $$
CREATE PROCEDURE pa_listar_datos_select_x_flag (flag INT)  
BEGIN
	IF flag	= 1	THEN	-- concepto	marca de tarjeta 
		SELECT correlativo AS codigo, descripcion FROM tblconcepto WHERE codConcepto = 1 AND correlativo > 0 AND marcaBaja = 0;
	END	IF;
	
	IF flag	= 2	THEN	-- concepto	tipo de	tarjeta	
		SELECT correlativo AS codigo, descripcion FROM tblconcepto WHERE codConcepto = 2 AND correlativo > 0 AND marcaBaja = 0;
	END	IF;
	
	IF flag	= 3	THEN	-- concepto	estado descuento 
		SELECT correlativo AS codigo, descripcion FROM tblconcepto WHERE codConcepto = 3 AND correlativo > 0 AND marcaBaja = 0;
	END	IF;

	 IF	flag = 4 THEN	-- Listar Bancos
		SELECT codBanco,abreviatura, descripcion from tblbanco WHERE marcaBaja = 0;
	END	IF;
	
	IF flag	= 5	THEN	-- Listar Categorias
		SELECT codCategoria	as codigo, descCategoria as	descripcion	from tblCategoria WHERE	marcaBaja =	0;
	END	IF;
	
END
$$
DELIMITER ;


-- --------------Listar	descuentos por filtro---------------------

DROP PROCEDURE IF EXISTS pa_descuento_listar;
DELIMITER $$
CREATE PROCEDURE pa_descuento_listar(
	codMarca INT,
	codTipo	INT,
	codBanco VARCHAR(10),
	codCategoria INT,
	codEstado INT,
	fechaI DATE,
	fechaF DATE
)
BEGIN
DECLARE	consulta VARCHAR(2000);
DECLARE	csl1 VARCHAR(20);

SET	consulta = 
	'SELECT	ntraDescuento, descripcion,	fechaInicial, fechaFin,	horaInicial, horaFin, IFNULL(porcentaje,0) as porcentaje, codEstado, desEstado,codMarca, desMarca, codTipo,	desTipo, codBanco, abrBanco, desBanco, codCategoria, descCategoria FROM	v_descuento_fitro2 WHERE tipoDescuento = 1 ';

	IF codMarca	!= 0 THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codMarca = ',	codMarca);
		END;
	END	IF;
	IF codTipo != 0	THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codTipo =	', codTipo);
		END;
	END	IF;
	IF codEstado !=	0 THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codEstado	= ', codEstado);
		END;
	END	IF;
	IF codBanco	!= '0' THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codBanco = ',	'''', codBanco,	'''');
		END;
	END	IF;
	IF fechaI != 0 AND fechaF != 0 THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND fechaInicial >= ', '''', fechaI, '''', ' AND fechaFin	<= ', '''',	fechaF,'''');
		END;
	END	IF;
	IF codCategoria	!= 0 THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codCategoria	= ', codCategoria);
		END;
	END	IF;

SET	@query = consulta;
PREPARE	strsql FROM	@query;
EXECUTE	strsql;

END
$$
DELIMITER ;

-- --------------Listar	descuentos ESPECIAL	---------------------

DROP PROCEDURE IF EXISTS pa_descuento_especial_listar;
DELIMITER $$
CREATE PROCEDURE pa_descuento_especial_listar(
	codBanco VARCHAR(10),
	codCategoria INT
)
BEGIN
DECLARE	consulta VARCHAR(2000);
DECLARE	csl1 VARCHAR(20);

SET	consulta = 
	'SELECT	
		ntraDescuento, 
		descripcion, 
		fechaInicial, 
		fechaFin, 
		horaInicial, 
		horaFin, 
		IFNULL(porcentaje,0) as	porcentaje,	
		codEstado, 
		desEstado,
		codMarca, 
		desMarca, 
		codTipo, 
		desTipo, 
		codBanco, 
		abrBanco, 
		desBanco, 
		codCategoria, 
		descCategoria 
	FROM v_descuento_fitro2	
	WHERE tipoDescuento	= 2	';

	IF codEstado !=	0 THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codEstado	= ', codEstado);
		END;
	END	IF;
	IF codBanco	!= '0' THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codBanco = ',	'''', codBanco,	'''');
		END;
	END	IF;
	IF codCategoria	!= 0 THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codCategoria	= ', codCategoria);
		END;
	END	IF;

SET	@query = consulta;

PREPARE	strsql FROM	@query;
EXECUTE	strsql;

END
$$
DELIMITER ;

-- --------------Registrar/	Editar descuento---------------------
DROP PROCEDURE IF EXISTS pa_descuento_reg_edit;
DELIMITER $$
CREATE PROCEDURE pa_descuento_reg_edit(
	proceso	int,
	fechaI date,
	fechaF date,
	horaI time,
	horaF time,
	codestado int,
	marca int,
	tipo int,
	banco varchar(10),
	categoria int,
	cantidad decimal(6,2),
	descrip	varchar(250),
	ntradesc int,
	nomUsuario varchar(20)
)
BEGIN

	DECLARE	idnuevo	INT;
	DECLARE	mensaje	VARCHAR(150);
	DECLARE	flag INT;
	DECLARE	codigo INT;
	DECLARE	cant SMALLINT DEFAULT 0;
	
	IF proceso = 1 THEN
		SET	cant = 
			(
			SELECT COUNT(a.ntraDescuento) 
			FROM v_descuento_fitro1	AS a 
			WHERE a.marca =	marca 
			AND	a.tipoTarjeta =	tipo 
			AND	a.banco	= banco
			AND	a.categoria	= categoria
			AND	a.tipoDescuento	= 1
			AND	NOT	((fechaI < a.fechaInicial AND fechaF < a.fechaInicial) OR (fechaI >	a.fechaFin AND fechaF >	a.fechaFin))
			AND	NOT	((horaI	< a.horaInicial	AND	horaF <	a.horaInicial) OR (horaI > a.horaFin AND horaF > a.horaFin))
			);
		IF cant	= 0	THEN
			START TRANSACTION;
			BEGIN
				INSERT INTO	tbldescuentos (descripcion,	fechaInicial, fechaFin,	horaInicial, horaFin, estado, fechaProceso,	horaProceso, usuario, ip, mac, codSucursal)	
				VALUES (descrip, fechaI, fechaF, horaI,	horaF, codestado, CURDATE(), CURTIME(),	nomUsuario,	'',	'',	1);
			END;
			BEGIN
				SET	idnuevo	= LAST_INSERT_ID();
			END;
			BEGIN
				INSERT INTO	tbldetalledescuentos(ntraDescuento,	flag, valorInicial,	valorFinal,	fechaProceso, horaProceso, usuario,	ip,	mac)
				VALUES(idnuevo,	1, CONVERT(marca, char(10)), '', CURDATE(),	CURTIME(), nomUsuario, '', '');

				INSERT INTO	tbldetalledescuentos(ntraDescuento,	flag, valorInicial,	valorFinal,	fechaProceso, horaProceso, usuario,	ip,	mac)
				VALUES(idnuevo,	2, CONVERT(tipo, char(10)),	'',	CURDATE(), CURTIME(), nomUsuario, '', '');

				INSERT INTO	tbldetalledescuentos(ntraDescuento,	flag, valorInicial,	valorFinal,	fechaProceso, horaProceso, usuario,	ip,	mac)
				VALUES(idnuevo,	3, CONVERT(cantidad, char(10)),	'',	CURDATE(), CURTIME(), nomUsuario, '', '');

				INSERT INTO	tbldetalledescuentos(ntraDescuento,	flag, valorInicial,	valorFinal,	fechaProceso, horaProceso, usuario,	ip,	mac)
				VALUES(idnuevo,	4, banco, '', CURDATE(), CURTIME(),	nomUsuario,	'',	'');
				
				INSERT INTO	tbldetalledescuentos(ntraDescuento,	flag, valorInicial,	valorFinal,	fechaProceso, horaProceso, usuario,	ip,	mac)
				VALUES(idnuevo,	5, CONVERT(categoria, char(10)), '', CURDATE(),	CURTIME(), nomUsuario, '', '');
			END;
			COMMIT;
			SET	mensaje	= 'SE REGISTRO CON EXITO';
			SET	flag = 0;
			SET	codigo = idnuevo;
		ELSE
			SET	mensaje	= 'REGISTRO	YA EXISTE';
			SET	flag = -1;
			SET	codigo = 0;
		END	IF;
		SELECT flag, mensaje, codigo;
	END	IF;
	
	IF proceso = 2 THEN
		SET	cant = 
			(
			SELECT COUNT(a.ntraDescuento) 
			FROM v_descuento_fitro1	AS a 
			WHERE a.marca =	marca 
			AND	a.tipoTarjeta =	tipo 
			AND	a.banco	= banco
			AND	a.categoria	= categoria
			AND	a.tipoDescuento	= 1
			AND	NOT	((fechaI < a.fechaInicial AND fechaF < a.fechaInicial) OR (fechaI >	a.fechaFin AND fechaF >	a.fechaFin))
			AND	NOT	((horaI	< a.horaInicial	AND	horaF <	a.horaInicial) OR (horaI > a.horaFin AND horaF > a.horaFin))
			AND	ntraDescuento <> ntradesc
			);
		IF cant	= 0	THEN
			START TRANSACTION;
			BEGIN
				UPDATE tbldescuentos SET descripcion = descrip,	fechaInicial = fechaI, fechaFin	= fechaF, horaInicial =	horaI, horaFin = horaF,	estado = codestado,	usuario= nomUsuario	WHERE ntraDescuento	= ntradesc;
			END;
			BEGIN
				DELETE FROM	tbldetalledescuentos WHERE ntraDescuento = ntradesc;
			END;
			BEGIN
				INSERT INTO	tbldetalledescuentos(ntraDescuento,	flag, valorInicial,	valorFinal,	fechaProceso, horaProceso, usuario,	ip,	mac)
				VALUES(ntradesc, 1,	CONVERT(marca, char(10)), '', CURDATE(), CURTIME(),	nomUsuario,	'',	'');

				INSERT INTO	tbldetalledescuentos(ntraDescuento,	flag, valorInicial,	valorFinal,	fechaProceso, horaProceso, usuario,	ip,	mac)
				VALUES(ntradesc, 2,	CONVERT(tipo, char(10)), '', CURDATE(),	CURTIME(), nomUsuario, '', '');

				INSERT INTO	tbldetalledescuentos(ntraDescuento,	flag, valorInicial,	valorFinal,	fechaProceso, horaProceso, usuario,	ip,	mac)
				VALUES(ntradesc, 3,	CONVERT(cantidad, char(10)), '', CURDATE(),	CURTIME(), nomUsuario, '', '');

				INSERT INTO	tbldetalledescuentos(ntraDescuento,	flag, valorInicial,	valorFinal,	fechaProceso, horaProceso, usuario,	ip,	mac)
				VALUES(ntradesc, 4,	banco, '', CURDATE(), CURTIME(), nomUsuario, '', '');
				
				INSERT INTO	tbldetalledescuentos(ntraDescuento,	flag, valorInicial,	valorFinal,	fechaProceso, horaProceso, usuario,	ip,	mac)
				VALUES(ntradesc, 5,	CONVERT(categoria, char(10)), '', CURDATE(), CURTIME(),	nomUsuario,	'',	'');
			END;
			COMMIT;
			SET	mensaje	= 'SE ACTUALIZO	CON	EXITO';
			SET	flag = 0;
			SET	codigo = ntradesc;
		ELSE
			SET	mensaje	= 'REGISTRO	YA EXISTE';
			SET	flag = -1;
			SET	codigo = 0;
		END	IF;
		SELECT flag, mensaje, codigo;
	END	IF;

END
$$
DELIMITER ;

-- --------------Activar / Desactivar Descuento---------------------
DROP PROCEDURE IF EXISTS pa_descuento_activar_desactivar;
DELIMITER $$
CREATE PROCEDURE pa_descuento_activar_desactivar(
	ntraDesc INT,
	codEstado INT
)  
BEGIN
	DECLARE	mensaje	VARCHAR(100);
	DECLARE	flag INT;
	DECLARE	codigoDesc INT;

	START TRANSACTION;
		BEGIN
			UPDATE tbldescuentos SET estado	= codEstado	WHERE ntraDescuento	= ntraDesc;
		END;
	COMMIT;
	BEGIN
		IF codEstado = 1 THEN
			SET	mensaje	= 'SE ACTIVO CON EXITO';
		ELSE
			SET	mensaje	= 'SE DESACTIVO	CON	EXITO';
		END	IF;
	END;
	BEGIN
		SET	flag = 0;
		SET	codigoDesc = ntraDesc;
		SELECT flag, mensaje, codigoDesc;
	END;
END
$$
DELIMITER ;


-- -------------- Eliminar Descuento---------------------

DROP PROCEDURE IF EXISTS pa_descuento_eliminar_descuento;
DELIMITER $$
CREATE PROCEDURE pa_descuento_eliminar_descuento(
	ntraDesc INT
)  
BEGIN
	DECLARE	mensaje	VARCHAR(100);
	DECLARE	flag INT DEFAULT 0;
	DECLARE	codigoDesc INT;

	START TRANSACTION;
		BEGIN
			UPDATE tbldescuentos SET marcaBaja = 9 WHERE ntraDescuento = ntraDesc;
			
			IF ROW_COUNT() > 0 THEN
				SET	flag = 1;
			ELSE
				SET	flag = 0;
			END	IF;
		END;
	COMMIT;
	BEGIN
		SELECT flag;
	END;
END
$$
DELIMITER ;


-- --------------REGISTRAR USUARIO Y PASSWORD DEL USUARIO---------------------

DROP PROCEDURE IF EXISTS pa_registrar_user;
DELIMITER $$
CREATE PROCEDURE pa_registrar_user(nomUser varchar(50),	pass varchar(50), codPersona int)
BEGIN
	DECLARE	aux	int	DEFAULT	0;
	DECLARE	mensaje	varchar(50)	DEFAULT	'';
	
	SET	aux	= (SELECT COUNT(ntraUsuario) FROM tblusuario WHERE users = nomUser);
	
	IF aux > 0 THEN
		SET	mensaje	= 'USUARIO YA EXISTE';
	ELSE
		INSERT INTO	tblusuario (users, password, estado, codPerfil,	codPersona,	codSucursal, fechaProceso, horaProceso,	usuario, ip, mac) 
		VALUES (nomUser, AES_ENCRYPT(pass,'AES'), 1, 1,	codPersona,	1, CURDATE(), CURTIME(),'','','');
		SET	mensaje	= 'USUARIO REGISTRADO CON EXITO';
	END	IF;
	SELECT mensaje;
END
$$
DELIMITER ;


-- --------------VERIFICAR LOGIN DEL USUARIO---------------------

DROP PROCEDURE IF EXISTS pa_verificar_user;
DELIMITER $$
CREATE PROCEDURE pa_verificar_user(
	nomUser	varchar(50), 
	pass varchar(50)
)
BEGIN
	DECLARE	cant int DEFAULT 0;
	DECLARE	ntraUsu	int	DEFAULT	0;
	DECLARE	passReal varchar(30) DEFAULT '';
	DECLARE	respuesta int DEFAULT 0;
	DECLARE	mensaje	varchar(200);
	DECLARE	aux	int	DEFAULT	0;
	DECLARE	perfil varchar(20) DEFAULT '';
	DECLARE	fkcodPersona int DEFAULT 0;
	DECLARE	nombres	VARCHAR(50)	DEFAULT	'';
	
	SELECT 
		COUNT(u.ntraUsuario),
		u.ntraUsuario,
		(SELECT	p.descripcion from tblperfil p WHERE u.codPerfil = p.codigo	AND	p.marcaBaja	= 0),
		u.codPersona,
		(SELECT	CONCAT(nombres,	' ', apellidoPaterno, '	', apellidoMaterno)	FROM tblPersona	per	WHERE per.codPersona = u.codPersona	AND	per.marcaBaja =	0)
	INTO 
		cant,
		ntraUsu,
		perfil,
		fkcodPersona,
		nombres
	FROM tblusuario	u
	WHERE marcaBaja	= 0
		AND	estado = 1
		AND	users =	nomUser
		AND	(select	pass like binary AES_DECRYPT( password,'AES')) > 0;
	
	IF cant	> 0	THEN
		BEGIN
			SET	mensaje	= 'ACCESO CORRECTO';
			SET	respuesta =	0;
		END;
	END	IF;
	 
	IF cant	= 0	THEN
		BEGIN
			SET	mensaje	= 'CREDENCIALES	INCORRECTAS';
			SET	respuesta =	-1;
		END;
	END	IF;
	
	SELECT ntraUsu as ntraUsuario, nomUser AS nombre, pass as password,	 respuesta,	mensaje, perfil, fkcodPersona;
END
$$
DELIMITER ;


-- -------------- OBTENER DESCUENTO	TARJETA	---------------------

DROP PROCEDURE IF EXISTS pa_dbcanasteria_vt_obtener_descuentoTarjeta;
DELIMITER $$
CREATE PROCEDURE `pa_dbcanasteria_vt_obtener_descuentoTarjeta`(IN `p_codigo_BIN` VARCHAR(3000),IN p_categoria INT, 
IN `p_fecha` CHAR(10), IN `p_hora` CHAR(8))
BEGIN
DECLARE	l_marca	varchar(45);
DECLARE	l_tipo TINYINT(1);
DECLARE	l_banco	varchar(100);


	SELECT marca, tarjeta, banco
	INTO l_marca, l_tipo, l_banco
	FROM tblCodBIN
	WHERE marcaBaja	= 0
	AND	sha2(codBIN,224) = p_codigo_BIN
	;



SELECT CODIGO, DESCRIPCION,	PORCENTAJE
FROM 
	(
	SELECT 
		A.ntraDescuento	AS CODIGO,
		A.descripcion AS DESCRIPCION,
		(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 4 AND A1.ntraDescuento =	A.ntraDescuento) AS	BANCO,
		(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 1 AND A1.ntraDescuento =	A.ntraDescuento) AS	MARCA,
		(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 2 AND A1.ntraDescuento =	A.ntraDescuento) AS	TIPO,
		(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 3 AND A1.ntraDescuento =	A.ntraDescuento) AS	PORCENTAJE,
		(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 5 AND A1.ntraDescuento =	A.ntraDescuento) AS	CATEGORIA
	FROM tblDescuentos AS A
	WHERE A.marcaBaja =	0
	AND	A.estado = 1
	AND	cast(p_fecha AS	date) BETWEEN A.fechaInicial AND A.fechaFin
	AND	cast(p_hora	AS TIME) BETWEEN A.horaInicial AND A.horaFin
	AND	A.tipoDescuento	= 1
	AND	A.codSucursal =	1
	) AS T
WHERE T.BANCO =	l_banco
AND	T.MARCA	= l_marca
AND	T.TIPO = l_tipo
AND	T.CATEGORIA	= p_categoria

;
END$$
DELIMITER ;


-- --------------------------- CONSULTAR DESCUENTO	-------------------------

DROP PROCEDURE IF EXISTS pa_dbcanasteria_consultar_descuentos;
DELIMITER $$
CREATE PROCEDURE pa_dbcanasteria_consultar_descuentos(
	p_fecha	CHAR(10), 
	p_hora CHAR(8),
	p_cbin VARCHAR(3000),
	p_xml_deta TEXT
)
proc_label:BEGIN

DECLARE	i INT;				 --	Iteraciones
DECLARE	j INT;
DECLARE	l_band INT;			 --	Bandera
DECLARE	l_flag BOOLEAN;		 --	Flag
DECLARE	l_coun INT;			 --	Cantidad de	Descuentos tipo	2
DECLARE	l_cont INT;			 --	Contador
DECLARE	l_con1 INT;			 --	Contador
DECLARE	l_deli INT;			 --	Delimitador
DECLARE	l_item INT;			 --	Item del Articulo
DECLARE	l_ccat VARCHAR(500); --	Cadena de Categorias
DECLARE	l_codi INT;			 --	Codigo Descuento
DECLARE	l_codc INT;			 --	Codigo Categoria
DECLARE	l_codb VARCHAR(20);	 --	Codigo Banco
DECLARE	l_ttar INT;			 --	Codigo Tipo	Tarjeta
DECLARE	l_codm INT;			 --	Codigo Marca
DECLARE	l_tdes INT;			 --	Tipo de	Descuento
DECLARE	l_desc VARCHAR(500); --	Descripcion	Descuento
DECLARE	l_desd VARCHAR(500); --	Descripcion	Descuento
DECLARE	l_desc_codc	VARCHAR(50); --	Descripcion	Categoria
DECLARE	l_desc_codb	VARCHAR(50); --	Descripcion	Banco
DECLARE	l_desc_ttar	VARCHAR(50); --	Descripcion	Tipo Tarjeta
DECLARE	l_desc_codm	VARCHAR(50); --	Descripcion	Marca
DECLARE	l_porc FLOAT(19,2);	 --	Porcentaje de Descuento
DECLARE	l_irow VARCHAR(500); --	Cadena para	almacenar la fila
DECLARE	l_leng INT;			 --	Tama�o de la cadena
DECLARE	fin	INTEGER	DEFAULT	0;

DECLARE	l_marca	INT;
DECLARE	l_tipo INT;
DECLARE	l_banco	VARCHAR(20);

DROP TABLE IF EXISTS tmp_productos;
DROP TABLE IF EXISTS tmp_categorias;
DROP TABLE IF EXISTS tmp_descuentos;
DROP TABLE IF EXISTS tmp_coincidencias;
DROP TABLE IF EXISTS tmp_auxiliar;
DROP TABLE IF EXISTS tmp_dscto;
DROP TABLE IF EXISTS tmp_descuentos_aux1;
DROP TABLE IF EXISTS tmp_descuentos_aux2;

CREATE TEMPORARY TABLE tmp_productos( item INT,	categorias VARCHAR(500)	);
CREATE TEMPORARY TABLE tmp_categorias( item	INT, categoria VARCHAR(500)	);
CREATE TEMPORARY TABLE tmp_descuentos( item	INT, codc INT, tdes	INT, codb VARCHAR(10), ttar	INT, codm INT, codi	INT, desd VARCHAR(500),	porc FLOAT(19,2) );
CREATE TEMPORARY TABLE tmp_descuentos_aux1(	item INT, codc INT,	tdes INT, codb VARCHAR(10),	ttar INT, codm INT,	codi INT, desd VARCHAR(500), porc FLOAT(19,2) );
CREATE TEMPORARY TABLE tmp_descuentos_aux2(	item INT, codc INT,	tdes INT, codb VARCHAR(10),	ttar INT, codm INT,	codi INT, desd VARCHAR(500), porc FLOAT(19,2) );
CREATE TEMPORARY TABLE tmp_coincidencias( codb VARCHAR(10),	ttar INT );
CREATE TEMPORARY TABLE tmp_auxiliar( codb VARCHAR(10), ttar	INT	);
CREATE TEMPORARY TABLE tmp_dscto( codb VARCHAR(10),	ttar INT );
	
	SET	l_cont = (extractValue(p_xml_deta, 'count(/detalle/producto)'));
	SET	i =	0;
	
	WHILE i	< l_cont DO
		SET	i =	i +	1;
		SET	l_irow = CONCAT('/detalle/producto[', i	, ']');
		SET	l_item = EXTRACTVALUE(p_xml_deta, concat(l_irow, '/item'));
		SET	l_ccat = EXTRACTVALUE(p_xml_deta, concat(l_irow, '/categoria'));
		
		INSERT INTO	tmp_productos (item, categorias) VALUES	(l_item, l_ccat);
		
		-- Desarmar	categorias
		SET	l_flag = TRUE;
		SET	l_con1 = 0;
		WHILE l_flag DO
			SET	l_leng = (SELECT LENGTH(l_ccat));
			SET	l_codc = (SELECT SUBSTRING_INDEX(l_ccat,'|',1));
			SET	l_deli = (SELECT LOCATE('|',l_ccat));
			INSERT INTO	tmp_categorias (item, categoria) VALUES	(l_item, l_codc);
			SET	l_ccat = (SELECT SUBSTRING(l_ccat,l_deli+1,l_leng-l_deli));
			IF l_ccat =	null OR	l_ccat = ""	THEN
				SET	l_flag = FALSE;
			END	IF;
			SET	l_con1 = l_con1	+ 1;
			IF l_con1 >	10 THEN
				SET	l_flag = FALSE;
			END	IF;
		END	WHILE;
		
		-- Obtener todos los descuentos
		INSERT INTO	tmp_descuentos (item, codc,	tdes, codb,	ttar, codm,	codi, desd,	porc)
		SELECT l_item, CATEGORIA, TIPO_DESCUENTO, BANCO, TIPO, MARCA, CODIGO, DESCRIPCION, PORCENTAJE
		FROM 
			(
			SELECT 
				A.ntraDescuento	AS CODIGO,
				A.descripcion AS DESCRIPCION,
				A.tipoDescuento	AS TIPO_DESCUENTO,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 4 AND A1.ntraDescuento =	A.ntraDescuento) AS	BANCO,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 1 AND A1.ntraDescuento =	A.ntraDescuento) AS	MARCA,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 2 AND A1.ntraDescuento =	A.ntraDescuento	LIMIT 1) AS	TIPO,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 3 AND A1.ntraDescuento =	A.ntraDescuento) AS	PORCENTAJE,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 5 AND A1.ntraDescuento =	A.ntraDescuento) AS	CATEGORIA
			FROM tblDescuentos AS A
			WHERE A.marcaBaja =	0
			AND	A.estado = 1
			AND	cast(p_fecha AS	date) BETWEEN A.fechaInicial AND A.fechaFin
			AND	cast(p_hora	AS TIME) BETWEEN A.horaInicial AND A.horaFin
			AND	A.codSucursal =	1
			) AS T
		WHERE T.CATEGORIA IN (SELECT categoria FROM	tmp_categorias WHERE item =	l_item);
		
		-- OBTENER TIPOS DE	TARJETAS ALGUNOS --
		SET	fin	= 0;
		BEGIN
			DECLARE	curs_tarj_algunos CURSOR FOR 
				SELECT item, codc, tdes, codb, ttar, codm, codi, desd, porc
				FROM tmp_descuentos
				WHERE ttar <> 999 AND tdes = 2;
			DECLARE	CONTINUE HANDLER FOR NOT FOUND SET fin = 1;
			OPEN curs_tarj_algunos;
			GET_RUNNERS: LOOP
				FETCH curs_tarj_algunos	INTO l_item, l_codc, l_tdes, l_codb, l_ttar, l_codm, l_codi, l_desd, l_porc;
				IF fin = 1 THEN
					LEAVE GET_RUNNERS;
				END	IF;
				
				INSERT INTO	tmp_descuentos_aux1	(item, codc, tdes, codb, ttar, codm, codi, desd, porc)
				SELECT l_item, l_codc, l_tdes, l_codb, valorInicial, l_codm, l_codi, l_desd, l_porc
				FROM tblDetalleDescuentos
				WHERE marcaBaja	= 0	AND	estadoDetalle =	1 AND flag = 2 AND ntraDescuento = l_codi;
			
			END	LOOP GET_RUNNERS;
			CLOSE curs_tarj_algunos;
		END;
		
		DELETE FROM	tmp_descuentos WHERE ttar <> 999 AND tdes =	2;
		
		INSERT INTO	tmp_descuentos
		SELECT * FROM tmp_descuentos_aux1;
		
		DELETE FROM	tmp_descuentos_aux1;
		
	END	WHILE;
	
	-- VERIFICAR SI	EXISTEN	DESCUENTOS --
	SET	l_coun = 0;
	SELECT COUNT(*)	INTO l_coun	FROM tmp_descuentos;
	
	IF l_coun >	0 THEN
		
		-- VERIFICAR SI	EXISTEN	DESCUENTOS TIPO	2 --
		SET	l_coun = 0;
		SELECT COUNT(*)	INTO l_coun	FROM tmp_descuentos	WHERE tdes = 2;
		
		-- EXISTEN DESCUENTOS TIPO 2 --
		IF l_coun >	0 THEN
				SET	l_band = 1;
				
				-- OBTENER TODAS LAS MARCAS	DE TARJETAS	PARA DESCUENTO TIPO	2 --
				INSERT INTO	tmp_descuentos_aux1
				SELECT a.item, a.codc, a.tdes, a.codb, b.ttar, a.codm, a.codi, a.desd, a.porc
				FROM (SELECT * FROM	tmp_descuentos WHERE tdes =	2 AND ttar = 999) AS a,	(SELECT	correlativo	AS ttar	FROM tblconcepto WHERE codConcepto = 2 AND marcaBaja = 0 AND correlativo > 0) AS b ;
				
				INSERT INTO	tmp_descuentos
				SELECT * FROM tmp_descuentos_aux1;
				
				-- QUITAMOS	LOS	DESCUENTOS QUE TIENEN EL FLAG DE TODOS COMO	TIPO DE	TARJETA	--
				DELETE FROM	tmp_descuentos WHERE tdes =	2 AND ttar = 999;
				
				-- VERIFICAR COINCIDENCIAS --
				SET	fin	= 0;
				BEGIN
					DECLARE	curs_productos CURSOR FOR 
						SELECT DISTINCT	item FROM tmp_productos;
					DECLARE	CONTINUE HANDLER FOR NOT FOUND SET fin = 1;
					OPEN curs_productos;
					GET_RUNNERS: LOOP
						FETCH curs_productos INTO l_item;
						IF fin = 1 THEN
							LEAVE GET_RUNNERS;
						END	IF;
						
						-- OBTENER DESCUENTOS X	ITEM --
						INSERT INTO	tmp_dscto
						SELECT codb, ttar
						FROM tmp_descuentos
						WHERE tdes = 2 AND item	= l_item;
						
						IF ROW_COUNT() > 0 THEN
							
							IF l_band =	1 THEN
								INSERT INTO	tmp_coincidencias
								SELECT codb, ttar FROM tmp_dscto;
							END	IF;
							
							-- OBTENER COINCIDENCIAS --
							INSERT INTO	tmp_auxiliar
							SELECT a.codb, a.ttar
							FROM tmp_dscto AS a	INNER JOIN tmp_coincidencias AS	b ON a.codb	= b.codb AND a.ttar	= b. ttar;
							
							-- LIMPIAR COINCIDENCIAS PARA VOLVER A GUARDAR LAS NUEVAS COINCIDENCIAS	--
							DELETE FROM	tmp_coincidencias;
							
							-- GUARDAR LAS NUEVAS COINCIDENCIAS	--
							INSERT INTO	tmp_coincidencias
							SELECT codb, ttar FROM tmp_auxiliar;
							
							-- LIMPIAR TABLA AUXILIAR --
							DELETE FROM	tmp_auxiliar;
							
							-- LIMPIAR TABLA DE	DESCUENTOS X ITEM
							DELETE FROM	tmp_dscto;
							
							SET	l_band = l_band	+ 1;
						END	IF;
						
					END	LOOP GET_RUNNERS;
					CLOSE curs_productos;
				END;
				
				-- VALIDAR SI AUN EXISTEN COINCIDENCIAS	--
				SET	l_coun = 0;
				SELECT COUNT(*)	INTO l_coun	FROM tmp_coincidencias;
				
				IF l_coun >	0 THEN
					-- QUITAR BANCOS QUE NO	COINCIDEN --
					DELETE FROM	tmp_descuentos 
					WHERE codb NOT IN (SELECT codb FROM	tmp_coincidencias);
					
					-- QUITAR TIPOS	DE TARJETAS	QUE	NO COINCIDEN --
					DELETE FROM	tmp_descuentos 
					WHERE ttar NOT IN (SELECT ttar FROM	tmp_coincidencias);
					
				-- SI NO EXISTEN COINCIDENCIAS ENTONCES	SALIR DEL BUBLE	--
				ELSE
					SET	l_item = 0;
					SET	l_desc_codc	= "";
					SET	l_tdes = 0;
					SET	l_desc_codb	= "";
					SET	l_desc_ttar	= "";
					SET	l_desc_codm	= "";
					SET	l_porc = 0;
					
					SELECT 2 as	l_cod_rpta,	"DESCUENTOS TIPO 2 NO COINCIDEN" as	l_desc_rpta, l_item, l_desc_codc, l_tdes, l_desc_codb, l_desc_ttar,	l_desc_codm, l_porc;
					LEAVE proc_label;
				END	IF;
				
		END	IF;
		
		SET	l_desc_codb	= "";
		SET	l_tdes = 0;
		
		-- OBTENEMOS LOS DATOS DEL BIN --
		IF p_cbin <> ""	THEN
			SELECT marca, tarjeta, banco
			INTO l_marca, l_tipo, l_banco
			FROM tblCodBIN
			WHERE marcaBaja	= 0
			AND	sha2(codBIN,224) = p_cbin
			;
			
			IF ROW_COUNT() > 0 THEN
				DELETE FROM	tmp_descuentos WHERE NOT (codb = l_banco AND ttar =	l_tipo AND codm	= l_marca) AND tdes	= 1;
				
				SELECT DISTINCT	IFNULL((SELECT descripcion FROM	tblBanco WHERE codBanco	= IFNULL(codb,"")),"") 
				INTO l_desc_codb 
				FROM tmp_descuentos	
				WHERE NOT (codb	= l_banco AND ttar = l_tipo	) AND tdes = 2 LIMIT 1;
				
				DELETE FROM	tmp_descuentos WHERE NOT (codb = l_banco AND ttar =	l_tipo ) AND tdes =	2;
				
				if ROW_COUNT() > 0 THEN
					SET	l_tdes = 2;
				end	if;
				
			ELSE
				
				DELETE FROM	tmp_descuentos where tdes =	1;
				
				SELECT DISTINCT	IFNULL((SELECT descripcion FROM	tblBanco WHERE codBanco	= IFNULL(codb,"")),"") 
				INTO l_desc_codb 
				FROM tmp_descuentos	
				WHERE tdes = 2 LIMIT 1;
				
				DELETE FROM	tmp_descuentos where tdes =	2;
				
				if ROW_COUNT() > 0 THEN
					SET	l_tdes = 2;
				end	if;
				
			END	IF;
		END	IF;
		
		-- VERIFICAR SI	EXISTEN	DESCUENTOS --
		SET	l_coun = 0;
		SELECT COUNT(*)	INTO l_coun	FROM tmp_descuentos;
		
		IF l_coun =	0 THEN
			SET	l_item = 0;
			SET	l_desc_codc	= "";
			-- SET l_tdes =	0;
			-- SET l_desc_codb = "";
			SET	l_desc_ttar	= "";
			SET	l_desc_codm	= "";
			SET	l_porc = 0;
			SELECT 1 AS	l_cod_rpta , "NO SE ENCONTRARON DESCUENTOS"	AS l_desc_rpta , l_item, l_desc_codc, l_tdes, l_desc_codb, l_desc_ttar,	l_desc_codm, l_porc;
			LEAVE proc_label;
		END	IF;
		
		-- CONTAR DESCUENTOS X ITEM	--
		SET	fin	= 0;
		BEGIN
			DECLARE	curs_conteo	CURSOR FOR SELECT item , codb, ttar, codm, count(codi) FROM	tmp_descuentos GROUP BY	item, codb,	ttar, codm;
			DECLARE	CONTINUE HANDLER FOR NOT FOUND SET fin = 1;
			OPEN curs_conteo;
			GET_RUNNERS: LOOP
				FETCH curs_conteo INTO l_item ,	l_codb,	l_ttar,	l_codm,	l_cont;
				IF fin = 1 THEN
					LEAVE GET_RUNNERS;
				END	IF;
				
				-- SI EXISTE MAS DE	1 DESCUENTO	X CONFIGURACION	DE TARJETA
				IF l_cont >	1 THEN
					-- OBTENER EL MINIMO DESCUENTO --
					SET	l_codi = NULL;
					
					select min(porc) into l_porc 
					from tmp_descuentos
					WHERE item = l_item	AND	codb = l_codb AND ttar = l_ttar	AND	codm = l_codm;
					
					SELECT codi	INTO l_codi
					FROM tmp_descuentos	
					WHERE porc = l_porc	AND	item = l_item AND codb = l_codb	AND	ttar = l_ttar AND codm = l_codm
					limit 1;
					
					DELETE FROM	tmp_descuentos 
					WHERE item = l_item	AND	codb = l_codb AND ttar = l_ttar	AND	codm = l_codm AND codi <> l_codi;
				END	IF;
				
			END	LOOP GET_RUNNERS;
			CLOSE curs_conteo;
		END;
		
		-- RESUMIR RESULTADOS --
		INSERT INTO	tmp_descuentos_aux2
		SELECT	item, codc,	tdes, codb,	0 AS ttar, codm, codi, desd, porc
		FROM tmp_descuentos	
		WHERE tdes = 2 
		GROUP BY item, codc, tdes, codb, codm, codi, desd, porc;
		
		DELETE FROM	tmp_descuentos WHERE tdes =	2 ;
		
		INSERT INTO	tmp_descuentos
		SELECT * FROM tmp_descuentos_aux2;
		
		-- RETORNAR	VALORES	--
		
		SELECT 
			0 AS l_cod_rpta,
			"APLICA	DESCUENTO" AS l_desc_rpta,
			b.item AS l_item, 
			concat(	b.codc , "|", ifnull((SELECT descCategoria FROM	tblCategoria WHERE codCategoria	= b.codc),"")) AS l_desc_codc, 
			b.tdes AS l_tdes, 
			IFNULL((SELECT descripcion FROM	tblBanco WHERE codBanco	= IFNULL(b.codb,"")),"")  AS l_desc_codb, 
			IFNULL((SELECT descripcion FROM	tblconcepto	WHERE codConcepto =	2 AND correlativo =	b.ttar AND correlativo > 0),"")	AS l_desc_ttar,	
			IFNULL((SELECT descripcion FROM	tblconcepto	WHERE codConcepto =	1 AND correlativo =	IFNULL(b.codm,0) AND correlativo > 0),"") AS l_desc_codm, 
			IFNULL(b.porc,0) AS	l_porc
		FROM tmp_descuentos	AS b 
		ORDER BY b.item	ASC;
		
	ELSE
		SET	l_item = 0;
		SET	l_desc_codc	= "";
		SET	l_tdes = 0;
		SET	l_desc_codb	= "";
		SET	l_desc_ttar	= "";
		SET	l_desc_codm	= "";
		SET	l_porc = 0;
		SELECT 1 AS	l_cod_rpta , "NO SE ENCONTRARON DESCUENTOS"	AS l_desc_rpta , l_item, l_desc_codc, l_tdes, l_desc_codb, l_desc_ttar,	l_desc_codm, l_porc;
	END	IF;
	
END$$
DELIMITER ;


-- -------------- REGISTRAR	LOG	WS ---------------------

DROP PROCEDURE IF EXISTS pa_dbcanasteria_vt_registrar_logws;
DELIMITER $$
CREATE PROCEDURE `pa_dbcanasteria_vt_registrar_logws`(in p_direurl varchar(200), IN	p_webserv varchar(200),	IN p_tipolog SMALLINT,
 in	p_flagerr SMALLINT,IN p_origerr	SMALLINT,IN	p_logcerr VARCHAR(400),IN p_descerr	TEXT, 
 IN	p_datentr TEXT,	IN p_datsali TEXT, IN p_ip VARCHAR(20))
BEGIN


INSERT INTO	tbllogws(direurl,webserv,tipolog,flagerr,origerr,logcerr,descerr,datentr,datsali,
											marBaja,fecProc,horaPro,usuario,ip)	
VALUES(p_direurl,p_webserv,p_tipolog,p_flagerr,p_origerr,p_logcerr,p_descerr,p_datentr,p_datsali,
			0,(select CURDATE()),(select curTime()),(select	current_user()),p_ip);
	
	SELECT LAST_INSERT_ID()	as last;
END$$
DELIMITER ;

-- ------------- ACTUALIZAR	CODIOG BIN ---------------------

DROP PROCEDURE IF EXISTS pa_actualizar_bin;
DELIMITER $$
CREATE PROCEDURE pa_actualizar_bin(bin varchar(16),	id int)
BEGIN
	UPDATE tblcodbin SET codBIN	= bin WHERE	ntraParametrizador = id;
END
$$
DELIMITER ;

-- ------------- LISTAR	CODIOG BIN ---------------------

DROP PROCEDURE IF EXISTS pa_bin_listar;
DELIMITER $$
CREATE PROCEDURE pa_bin_listar(codMarca	int, codBanco varchar(10), codTipo int)
BEGIN
DECLARE	consulta VARCHAR(2000);
DECLARE	csl1 VARCHAR(20);

SET	consulta = 
	'select	
		vi.ntraParametrizador, 
		vi.codBIN, 
		vi.idMarca,	
		vi.descMarca, 
		vi.codTarjeta, 
		vi.descTarjeta,	
		vi.codBanco, 
		vi.descBanco,
		vi.marcaBaja 
	from v_bin_filtros vi 
	where 1	= 1	and	vi.marcaBaja = 0';

	IF codMarca	!= 0 THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND idMarca =	', codMarca);
		END;
	END	IF;
	
	IF codBanco	!= '0' THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codBanco = ',	"'", codBanco, "'");
		END;
	END	IF;
	IF codTipo != 0	THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codTarjeta = ', codTipo);
		END;
	END	IF;


SET	@query = consulta;

PREPARE	strsql FROM	@query;
EXECUTE	strsql;

END
$$
DELIMITER ;

-- ------------- ELIMINAR CODIOG BIN ---------------------

DROP PROCEDURE IF EXISTS pa_eliminar_bin;
DELIMITER $$
CREATE PROCEDURE pa_eliminar_bin(id	int)
BEGIN
	UPDATE tblcodbin SET marcaBaja = 9	WHERE  ntraParametrizador =	id;
END
$$
DELIMITER ;

-- ------------- REGISTRAR CODIOG BIN ---------------------

DROP PROCEDURE IF EXISTS pa_registrar_BIN;
DELIMITER $$
CREATE PROCEDURE pa_registrar_BIN(codBIN varchar(16), marca	int, tarjeta int, banco	varchar(16), usuario varchar(20))
BEGIN
	DECLARE	cant int DEFAULT 0;
	DECLARE	codRespuesta SMALLINT DEFAULT 0;
	DECLARE	descRespuesta VARCHAR(100) DEFAULT "";
	
	SET	cant = (SELECT COUNT(codBIN) FROM tblcodbin	AS a WHERE a.codBIN	= codBIN AND a.marcaBaja = 0);
	
	IF cant	= 0	THEN
		INSERT INTO	tblcodbin (codBIN, marca, tarjeta, banco, fechaProceso,	horaProceso, usuario, ip, mac)
		VALUES (codBIN,	marca, tarjeta,	banco, CURDATE(), CURTIME(), usuario, '0.0.0.0', '00:00:00:00:00:00');
		SET	descRespuesta =	"REGISTRO CORRECTAMENTE";
		SET	codRespuesta = 1;
	ELSE
		SET	descRespuesta =	"REGISTRO YA EXISTE";
		SET	codRespuesta = 0;
	END	IF;
	SELECT descRespuesta, codRespuesta;
END
$$
DELIMITER ;

-- ------------- EXISTE	BIN	-----------------------------

DROP PROCEDURE IF EXISTS pa_existe_bin;
DELIMITER $$
CREATE PROCEDURE pa_existe_bin(codm	INT, ttar INT, codb	VARCHAR(16))
BEGIN
	DECLARE	consulta VARCHAR(2000);
	
	SET	consulta = 'select count(ntraParametrizador) AS	l_cant from	v_bin_filtros where	1 =	1 ';

	IF codm	<> 0 THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND idMarca =	', codm);
		END;
	END	IF;
	
	IF ttar	<> 0 THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codTarjeta = ', ttar);
		END;
	END	IF;
	
	IF codb	<> '0' THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codBanco = ',	"'", codb, "'");
		END;
	END	IF;


	SET	@query = consulta;

	PREPARE	strsql FROM	@query;
	EXECUTE	strsql;

END
$$
DELIMITER ;


-- ------------- REGISTRAR CATEGORIAS  ---------------------

DROP PROCEDURE IF EXISTS pa_dbcanasteria_vt_categoria;
DELIMITER $$
CREATE PROCEDURE `pa_dbcanasteria_vt_categoria`(IN p_codcat	INT, IN	p_descat VARCHAR(100), IN p_ip VARCHAR(20))
	BEGIN


	DECLARE	descripcion	varchar(300);
	DECLARE	codigo varchar(300);
	DECLARE	ierr int default 0;
	
	DECLARE	CONTINUE HANDLER FOR SQLEXCEPTION  SET ierr	= 1;
	DECLARE	CONTINUE HANDLER FOR SQLWARNING	 SET ierr =	1;
				
	INSERT INTO	tblcategoria(codCategoria, descCategoria, marcaBaja, fechaProceso, horaProceso,	usuario,ip)
	VALUES(p_codcat, upper(p_descat), 0,(select	CURDATE()),(select curTime()),(select current_user()),p_ip);
	IF	ierr=1 THEN		   
	  SET codigo= -1;
	  SET descripcion ="ERROR AL PROCESAR INSERT CATEGORIA..";
	ELSE
		SET	codigo = 0;
		SET	descripcion	= "EXITO";
	END	IF;
	
		   
	SELECT codigo, descripcion;
	
END$$
DELIMITER ;

-- ------------- LISTAR	TIPOS DE TARJETAS ---------------------

DROP PROCEDURE IF EXISTS pa_listar_tipos;
DELIMITER $$
CREATE PROCEDURE `pa_listar_tipos`(IN p_banco varchar(10)) 
BEGIN
	
	SELECT t.correlativo as	codigo,	t.descripcion as descripcion 
	FROM tblParametros AS p
		INNER JOIN tblBanco	AS b
		INNER JOIN (SELECT correlativo,	descripcion	FROM tblconcepto WHERE codConcepto = 2)	AS t 
	WHERE p.prefijo	= 5
	AND	p.string1 =	b.codBanco
	AND	p.smallint1	= t.correlativo
	AND	b.codBanco = p_banco
	;
	
END
$$
DELIMITER ;

-- ------------- ELIMINAR CATEGORIAS  ---------------------

DROP PROCEDURE IF EXISTS pa_dbcanasteria_vt_BajaCategoria;
DELIMITER $$
CREATE PROCEDURE `pa_dbcanasteria_vt_BajaCategoria`()
	BEGIN


	DECLARE	descripcion	varchar(300);
	DECLARE	codigo varchar(300);
	DECLARE	ierr int default 0;
	
	DECLARE	CONTINUE HANDLER FOR SQLEXCEPTION  SET ierr	= 1;
	DECLARE	CONTINUE HANDLER FOR SQLWARNING	 SET ierr =	1;
		
	DELETE FROM	tblcategoria WHERE 1=1;
		
	
	IF	ierr=1 THEN		   
	  SET codigo= -1;
	  SET descripcion ="ERROR AL PROCESAR BAJA DE CATEGORIAS..";
	ELSE
		SET	codigo = 0;
		SET	descripcion	= "EXITO";
	END	IF;
	
		   
	SELECT codigo, descripcion;
	
END$$
DELIMITER ;


-- ------------- REGISTRAR LOG WEB	---------------------

DROP PROCEDURE IF EXISTS pa_registrar_log_web;
DELIMITER $$
CREATE PROCEDURE `pa_registrar_log_web`(p_sistema VARCHAR(255),	p_modulo VARCHAR(255), p_opcion	VARCHAR(255) , p_tipo SMALLINT,	p_descripcion TEXT,	p_usuario varchar(20))
BEGIN
	
	INSERT INTO	tblerroresweb (sistema,	modulo,	opcion,	tipo, descripcion, fechaProceso, horaProceso, usuario) 
	VALUES (p_sistema, p_modulo, p_opcion, p_tipo, p_descripcion, CURDATE(), CURTIME(),	usuario);
	
	SELECT LAST_INSERT_ID()	as transaccion;
	
END$$
DELIMITER ;


-- ------------- OBTENER CANTIDAD DE DESCUENTOS	---------------------

DROP PROCEDURE IF EXISTS pa_dbcanasteria_vt_obtenerCantDsctos_xlistCategorias;
DELIMITER $$
CREATE PROCEDURE `pa_dbcanasteria_vt_obtenerCantDsctos_xlistCategorias`(
IN `p_xmlCategoria`	TEXT, 
IN `p_fecha` CHAR(10), IN `p_hora` CHAR(8))
BEGIN

DECLARE	cnt	INT;
DECLARE	ptr	int	DEFAULT	0;
DECLARE	rowPtr varchar(100);
DECLARE	categoria varchar(20);
DECLARE	l_codi INT;
DECLARE	l_desc VARCHAR(700); 
DECLARE	l_porc DECIMAL(14,2);
DECLARE	l_flag INT;
DECLARE	l_cant INT;
DECLARE	l_cantdsct INT;

SET	categoria =	'';
SET	cnt	= (extractValue(p_xmlCategoria,	'count(/ListCategorias/categoria)'));
SET	ptr	= 0;
SET	l_cantdsct = 0;
SET	l_cant = 0;

CREATE TEMPORARY TABLE tmp_categorias( codigo INT );

WHILE ptr <	cnt	do
	
	SET	ptr	= ptr +	1;
	SET	rowPtr = concat('/ListCategorias/categoria[', ptr, ']');
   
	SET	categoria =	extractValue(p_xmlCategoria, concat(rowPtr,	'/codigo'));
	
	INSERT INTO	tmp_categorias (codigo)	VALUES (categoria);
	
END	WHILE;	

	SET	l_cant = (SELECT COUNT(*)
	FROM 
		(
		SELECT 
			A.ntraDescuento	AS CODIGO,
			A.descripcion AS DESCRIPCION,
			(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 4 AND A1.ntraDescuento =	A.ntraDescuento) AS	BANCO,
			(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 1 AND A1.ntraDescuento =	A.ntraDescuento) AS	MARCA,
			(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 2 AND A1.ntraDescuento =	A.ntraDescuento) AS	TIPO,
			(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 3 AND A1.ntraDescuento =	A.ntraDescuento) AS	PORCENTAJE,
			(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 5 AND A1.ntraDescuento =	A.ntraDescuento) AS	CATEGORIA
		FROM tblDescuentos AS A
		WHERE A.marcaBaja =	0
		AND	A.estado = 1
		AND	cast(p_fecha AS	date) BETWEEN A.fechaInicial AND A.fechaFin
		AND	cast(p_hora	AS TIME) BETWEEN A.horaInicial AND A.horaFin
		AND	A.tipoDescuento	= 1
		AND	A.codSucursal =	1
		) AS T
	WHERE T.CATEGORIA IN (SELECT DISTINCT codigo FROM tmp_categorias));
	
	DROP TABLE tmp_categorias;
	
	SET	l_cantdsct =  l_cantdsct + l_cant;
	SELECT l_cantdsct;
END
$$ 
DELIMITER ;


-- ------------- OBTENER LISTA DE DESCUENTO	X CATEGORIA	Y BIN  ---------------------

DROP PROCEDURE IF EXISTS pa_dbcanasteria_vt_obtener_listDsctoxlistCategycodbin;
DELIMITER $$
CREATE PROCEDURE `pa_dbcanasteria_vt_obtener_listDsctoxlistCategycodbin`(
IN `p_xmlCategoria`	TEXT, IN `p_codigo_BIN`	VARCHAR(3000),
IN `p_fecha` CHAR(10), IN `p_hora` CHAR(8))
BEGIN

DECLARE	cnt	INT;
DECLARE	ptr	int	DEFAULT	0;
DECLARE	rowPtr varchar(100);
DECLARE	categoria varchar(20);
DECLARE	l_codi INT;
DECLARE	l_desc VARCHAR(700); 
DECLARE	l_porc DECIMAL(14,2);
DECLARE	l_cadena_descuentos	text;
DECLARE	l_xml_descuentos longtext;
DECLARE	cursor_List_isdone BOOLEAN DEFAULT FALSE;
DECLARE	fin	INTEGER	DEFAULT	0;
DECLARE	l_flag INT;
DECLARE	l_marca	varchar(45);
DECLARE	l_tipo TINYINT(1);
DECLARE	l_banco	varchar(100);

SET	categoria =	'';
SET	cnt	= (extractValue(p_xmlCategoria,	'count(/ListCategorias/categoria)'));
SET	ptr	= 0;
SET	l_cadena_descuentos	= '';
SET	l_xml_descuentos ='';


		SELECT marca, tarjeta, banco
		INTO l_marca, l_tipo, l_banco
		FROM tblCodBIN
		WHERE marcaBaja	= 0
		AND	sha2(codBIN,224) = p_codigo_BIN
		;

CREATE TEMPORARY TABLE tmp_categorias( codigo INT );

WHILE ptr <	cnt	do
	SET	l_flag = 0;
	SET	ptr	= ptr +	1;
	SET	rowPtr = concat('/ListCategorias/categoria[', ptr, ']');
   
	SET	categoria =	extractValue(p_xmlCategoria, concat(rowPtr,	'/codigo'));
	
	INSERT INTO	tmp_categorias (codigo)	VALUES (categoria);
	
END	WHILE;	

	SET	fin	= 0;
	BEGIN
		DECLARE	cursor_List	CURSOR FOR
		SELECT CODIGO, DESCRIPCION,	PORCENTAJE 
		FROM 
			(
			SELECT 
				A.ntraDescuento	AS CODIGO,
				A.descripcion AS DESCRIPCION,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 4 AND A1.ntraDescuento =	A.ntraDescuento) AS	BANCO,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 1 AND A1.ntraDescuento =	A.ntraDescuento) AS	MARCA,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 2 AND A1.ntraDescuento =	A.ntraDescuento) AS	TIPO,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 3 AND A1.ntraDescuento =	A.ntraDescuento) AS	PORCENTAJE,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 5 AND A1.ntraDescuento =	A.ntraDescuento) AS	CATEGORIA
			FROM tblDescuentos AS A
			WHERE A.marcaBaja =	0
			AND	A.estado = 1
			AND	cast(p_fecha AS	date) BETWEEN A.fechaInicial AND A.fechaFin
			AND	cast(p_hora	AS TIME) BETWEEN A.horaInicial AND A.horaFin
			AND	A.tipoDescuento	= 1
			AND	A.codSucursal =	1
			) AS T
		WHERE T.BANCO =	l_banco
		AND	T.MARCA	= l_marca
		AND	T.TIPO = l_tipo
		AND	T.CATEGORIA	IN (SELECT DISTINCT	codigo FROM	tmp_categorias);
		
	 DECLARE CONTINUE HANDLER FOR NOT FOUND	SET	fin	= 1;
	 OPEN cursor_List;
	 get_runners: LOOP
	  
	  FETCH	cursor_List	INTO l_codi, l_desc, l_porc;
	  IF fin = 1 THEN
		leave get_runners;
	  END IF;
		
	  SET l_flag = l_flag +1;
		SET	l_cadena_descuentos	= 
		CONCAT('<Descuento><codcat>',categoria,'</codcat><codigo>',l_codi,'</codigo><descripcion>',l_desc,'</descripcion><porcentaje>',l_porc,'</porcentaje></Descuento>');
		
		SET	l_xml_descuentos = CONCAT(l_xml_descuentos ,	l_cadena_descuentos);	

	END	LOOP get_runners;
	CLOSE cursor_List;
	END;
	
	DROP TABLE tmp_categorias;
	
	IF l_flag =	0 THEN
			SET	l_cadena_descuentos	= 
			CONCAT('<Descuento><codcat>',categoria,'</codcat><codigo>0</codigo><descripcion></descripcion><porcentaje>0</porcentaje></Descuento>');
			
			SET	l_xml_descuentos = CONCAT(l_xml_descuentos ,	l_cadena_descuentos);	
	END	IF;

 SET l_xml_descuentos =	CONCAT('<ListDescuento>',l_xml_descuentos,'</ListDescuento>');

	SELECT l_xml_descuentos;
END
$$ 
DELIMITER ;


-- ------------- OBTENER LISTA DE DESCUENTO	X CATEGORIA	 ---------------------

DROP PROCEDURE IF EXISTS pa_dbcanasteria_vt_obtener_listaDescuentoxlistaCategoria;
DELIMITER $$
CREATE PROCEDURE `pa_dbcanasteria_vt_obtener_listaDescuentoxlistaCategoria`(
IN `p_xmlCategoria`	TEXT, 
IN `p_fecha` CHAR(10), IN `p_hora` CHAR(8))
BEGIN

DECLARE	cnt	INT;
DECLARE	ptr	int	DEFAULT	0;
DECLARE	rowPtr varchar(100);
DECLARE	categoria varchar(20);
DECLARE	l_codi INT;
DECLARE	l_desc VARCHAR(700); 
DECLARE	l_porc DECIMAL(14,2);
DECLARE	l_cadena_descuentos	text;
DECLARE	l_xml_descuentos longtext;
DECLARE	cursor_List_isdone BOOLEAN DEFAULT FALSE;
DECLARE	fin	INTEGER	DEFAULT	0;
DECLARE	l_flag INT;
DECLARE	l_categorias VARCHAR(700);
DECLARE	l_consulta VARCHAR(2000);

SET	l_consulta = '';
SET	categoria =	'';
SET	cnt	= (extractValue(p_xmlCategoria,	'count(/ListCategorias/categoria)'));
SET	ptr	= 0;
SET	l_cadena_descuentos	= '';
SET	l_xml_descuentos ='';
SET	l_categorias = "";

CREATE TEMPORARY TABLE tmp_categorias( codigo INT );

WHILE ptr <	cnt	do
	SET	l_flag = 0;
	SET	ptr	= ptr +	1;
	SET	rowPtr = concat('/ListCategorias/categoria[', ptr, ']');
   
	SET	categoria =	extractValue(p_xmlCategoria, concat(rowPtr,	'/codigo'));
	
	INSERT INTO	tmp_categorias (codigo)	VALUES (categoria);
	
END	WHILE;	

	 SET fin = 0;
	BEGIN
		DECLARE	cursor_List	CURSOR FOR
		SELECT CODIGO, DESCRIPCION,	PORCENTAJE 
		FROM 
			(
			SELECT 
				A.ntraDescuento	AS CODIGO,
				A.descripcion AS DESCRIPCION,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 4 AND A1.ntraDescuento =	A.ntraDescuento) AS	BANCO,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 1 AND A1.ntraDescuento =	A.ntraDescuento) AS	MARCA,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 2 AND A1.ntraDescuento =	A.ntraDescuento) AS	TIPO,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 3 AND A1.ntraDescuento =	A.ntraDescuento) AS	PORCENTAJE,
				(SELECT	valorInicial FROM tblDetalleDescuentos AS A1 WHERE A1.marcaBaja	= 0	AND	A1.estadoDetalle = 1 AND A1.flag = 5 AND A1.ntraDescuento =	A.ntraDescuento) AS	CATEGORIA
			FROM tblDescuentos AS A
			WHERE A.marcaBaja =	0
			AND	A.estado = 1
			AND	cast(p_fecha AS	date) BETWEEN A.fechaInicial AND A.fechaFin
			AND	cast(p_hora	AS TIME) BETWEEN A.horaInicial AND A.horaFin
			AND	A.tipoDescuento	= 1
			AND	A.codSucursal =	1
			) AS T
		WHERE T.CATEGORIA IN (SELECT DISTINCT codigo FROM tmp_categorias);
		
	 DECLARE CONTINUE HANDLER FOR NOT FOUND	SET	fin	= 1;
	 OPEN cursor_List;
	 get_runners: LOOP
	  
	  FETCH	cursor_List	INTO l_codi, l_desc, l_porc;
	  IF fin = 1 THEN
		leave get_runners;
	  END IF;
		
	  SET l_flag = l_flag +1;
		SET	l_cadena_descuentos	= 
		CONCAT('<Descuento><codcat>',categoria,'</codcat><codigo>',l_codi,'</codigo><descripcion>',l_desc,'</descripcion><porcentaje>',l_porc,'</porcentaje></Descuento>');
		
		SET	l_xml_descuentos = CONCAT(l_xml_descuentos ,	l_cadena_descuentos);	

	END	LOOP get_runners;
	CLOSE cursor_List;
	END;
	
	DROP TABLE tmp_categorias;
	
	IF l_flag =	0 THEN
			SET	l_cadena_descuentos	= 
			CONCAT('<Descuento><codcat>',categoria,'</codcat><codigo>0</codigo><descripcion></descripcion><porcentaje>0</porcentaje></Descuento>');
			
			SET	l_xml_descuentos = CONCAT(l_xml_descuentos ,	l_cadena_descuentos);	
	END	IF;

	SET	l_xml_descuentos = CONCAT('<ListDescuento>',l_xml_descuentos,'</ListDescuento>');

	SELECT l_xml_descuentos;
END
$$ 
DELIMITER ;




-- ------------- REGISTRAR DESCUENTO ESPECIAL  ---------------------

DROP PROCEDURE IF EXISTS pa_registrar_descuento_especial;
DELIMITER $$
CREATE PROCEDURE `pa_registrar_descuento_especial`(
	codDescuento VARCHAR(20),
	descripcion	VARCHAR(200),
	fechaInicial DATE,
	fechaFin DATE,
	horaInicial	TIME,
	horaFin	TIME,
	flagestado INT,
	tipoTarjeta	INT,
	codBanco VARCHAR(15),
	codCategoria INT,
	usuario	VARCHAR(50),
	cod_tarjeta_xml	TEXT
)
BEGIN
	
	DECLARE	idnuevo	INT;
	DECLARE	flag_detalle INT;
	DECLARE	mensaje	VARCHAR(150);
	DECLARE	codigo INT;
	DECLARE	cant SMALLINT DEFAULT 0;
	DECLARE	cant_categoria SMALLINT	DEFAULT	0;
	DECLARE	l_cdes INT;
	
	-- VARIABLES XML --
	DECLARE	i INT;
	DECLARE	l_cont INT;
	DECLARE	l_irow VARCHAR(500);
	DECLARE	l_dato INT;
	
	IF (codDescuento = "") THEN
		SET	l_cdes = 0;
	ELSE
		SET	l_cdes = codDescuento;
	END	IF;
	
	IF (tipoTarjeta	= 1) THEN -- TODAS
		SET	flag_detalle = 0;
	ELSE --	ALGUNOS
		SET	flag_detalle = 1;
	END	IF;
	
	SET	cant_categoria =
		(
		SELECT COUNT(a.ntraDescuento) 
		FROM v_descuento_fitro1	AS a 
		WHERE a.categoria =	codCategoria
		AND	a.tipoDescuento	= 1
		AND	a.ntraDescuento	<> l_cdes
		);
	
	IF (cant_categoria = 0)	THEN
		SET	cant = 
			(
			SELECT COUNT(a.ntraDescuento) 
			FROM v_descuento_fitro1	AS a 
			WHERE a.banco =	codBanco
			AND	a.categoria	= codCategoria
			AND	a.tipoDescuento	= 2
			AND	NOT	((fechaInicial < a.fechaInicial	AND	fechaFin < a.fechaInicial) OR (fechaInicial	> a.fechaFin AND fechaFin >	a.fechaFin))
			AND	NOT	((horaInicial <	a.horaInicial AND horaFin <	a.horaInicial) OR (horaInicial > a.horaFin AND horaFin > a.horaFin))
			AND	a.ntraDescuento	<> l_cdes
			);

		IF cant	= 0	THEN
			START TRANSACTION;
				IF (l_cdes = 0)	THEN
					BEGIN
						INSERT INTO	tbldescuentos (descripcion,	fechaInicial, fechaFin,	horaInicial, horaFin, tipoDescuento, estado, fechaProceso, horaProceso,	usuario, ip, mac, codSucursal) 
						VALUES (descripcion, fechaInicial, fechaFin, horaInicial, horaFin, 2, flagestado, CURDATE(), CURTIME(),	usuario, '', '', 1);
					END;
					BEGIN
						SET	idnuevo	= LAST_INSERT_ID();
					END;
				ELSE
					BEGIN
						UPDATE tbldescuentos
						SET	fechaInicial = fechaInicial, fechaFin =	fechaFin, horaInicial =	horaInicial, horaFin = horaFin,	estado = flagestado, 
							fechaProceso = CURDATE(), horaProceso =	CURTIME(), usuario = usuario, ip = '', mac = '', codSucursal = 1
						WHERE ntraDescuento	= l_cdes;
					END;
					BEGIN
						DELETE FROM	tblDetalleDescuentos WHERE ntraDescuento = l_cdes;
					END;
					BEGIN
						SET	idnuevo	= l_cdes;
					END;
				END	IF;
				BEGIN
					IF (tipoTarjeta	= 999) THEN
						INSERT INTO	tblDetalleDescuentos (ntraDescuento, flag, valorInicial, valorFinal, detalle, estadoDetalle, fechaProceso, horaProceso,	usuario	)
						VALUES (idnuevo, 2,	tipoTarjeta, tipoTarjeta, flag_detalle,	1, CURDATE(), CURTIME(), usuario);
					ELSE
						IF cod_tarjeta_xml = ""	THEN
							INSERT INTO	tblDetalleDescuentos (ntraDescuento, flag, valorInicial, valorFinal, detalle, estadoDetalle, fechaProceso, horaProceso,	usuario	)
							VALUES (idnuevo, 2,	tipoTarjeta, tipoTarjeta, flag_detalle,	1, CURDATE(), CURTIME(), usuario);
						ELSE
							SET	l_cont = (extractValue(cod_tarjeta_xml,	'count(/tarjetas/tarjeta)'));
							SET	i =	0;
							WHILE i	< l_cont DO
								SET	i =	i +	1;
								SET	l_irow = CONCAT('/tarjetas/tarjeta[', i	,']');
								SET	l_dato = EXTRACTVALUE(cod_tarjeta_xml, concat(l_irow, '/codigo'));
								
								INSERT INTO	tblDetalleDescuentos (ntraDescuento, flag, valorInicial, valorFinal, detalle, estadoDetalle, fechaProceso, horaProceso,	usuario	)
								VALUES (idnuevo, 2,	l_dato,	tipoTarjeta, flag_detalle, 1, CURDATE(), CURTIME(),	usuario);
							END	WHILE;
						END	IF;
					END	IF;
					
					INSERT INTO	tblDetalleDescuentos (ntraDescuento, flag, valorInicial, valorFinal, detalle, estadoDetalle, fechaProceso, horaProceso,	usuario	)
					VALUES (idnuevo, 4,	codBanco, codBanco,	0, 1, CURDATE(), CURTIME(),	usuario);
					
					INSERT INTO	tblDetalleDescuentos (ntraDescuento, flag, valorInicial, valorFinal, detalle, estadoDetalle, fechaProceso, horaProceso,	usuario	)
					VALUES (idnuevo, 5,	codCategoria, codCategoria,	0, 1, CURDATE(), CURTIME(),	usuario);
				END;
			COMMIT;
			SET	mensaje	= "REGISTRO	EXITOSO";
			SET	codigo = idnuevo;
		ELSE
			SET	mensaje	= "REGISTRO	YA EXISTE";
			SET	codigo = 0;
		END	IF;
	ELSE
		SET	mensaje	= "CATEGORIA YA	EXISTE EN DESCUENTO	NORMAL";
		SET	codigo = 0;
	END	IF;
	
	SELECT mensaje,	codigo;
	
END$$
DELIMITER ;


-- ------------- LISTAR	DESCUENTO ESPECIAL ---------------------

DROP PROCEDURE IF EXISTS pa_listar_descuento_especial;
DELIMITER $$
CREATE PROCEDURE pa_listar_descuento_especial(categoria	int, banco varchar(20))
BEGIN

	DECLARE	consulta VARCHAR(2000);

	SET	consulta = 
		'select	
			dct.ntraDescuento, 
			dct.descripcion, 
			dct.fechaInicial, 
			dct.fechaFin, 
			dct.horaInicial, 
			dct.horaFin, 
			dct.estado,	
			dct.descEstado,
			dct.tipoDescuento,
			dct.descTipoDescuento,
			dct.tipoTarjeta,
			(CASE WHEN dct.tipoTarjeta = 999 THEN "TODAS" ELSE "ALGUNAS" END) as descTipoTarjeta,
			dct.banco,
			(select	descripcion	from tblbanco where	codBanco = dct.banco AND marcaBaja = 0)	AS descBanco,
			dct.categoria,
			(select	descCategoria from tblcategoria	where codCategoria = dct.categoria and marcaBaja = 0) as descCategoria,
			(select	GROUP_CONCAT(dt.valorInicial SEPARATOR "|")	from tbldetalledescuentos dt where dt.ntraDescuento	= dct.ntraDescuento	and	dt.flag	= 2) as	tiposTarjetas
		from v_descuento_fitro1	dct	
		where dct.tipoDescuento	= 2';

	IF banco <>	'0'	THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND dct.banco	= ', "'", banco, "'");
		END;
	END	IF;

	IF categoria !=	0 THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND dct.categoria	= ', categoria);
		END;
	END	IF;
	
	SET	@query = consulta;

	PREPARE	strsql FROM	@query;
	EXECUTE	strsql;
END
$$
DELIMITER ;


-- -----------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS pa_dbcanasteria_vt_mostrar_ventas;
DELIMITER $$
CREATE PROCEDURE `pa_dbcanasteria_vt_mostrar_ventas`(IN	`p_fechain`	CHAR(10),IN	`p_fechafi`	CHAR(10),
 IN	`p_banco` VARCHAR(30),IN `p_categoria` INT)
BEGIN
DECLARE	consulta VARCHAR(2000);

SET	consulta = 
	'select	
		vi.codvent,	vi.fechped,	vi.desarti,	vi.categoria, IFNULL(vi.descbanco,"")AS	descbanco, IFNULL(vi.tarjeta,"")AS tarjeta,cantive,IFNULL(importe,0)AS importe,
		IFNULL(impdscto,0) AS impdscto,IFNULL(impvent,0) AS	impvent
	from v_reporte_Ventas vi 
	where 1	= 1';

	IF p_fechain <>	"" THEN
		BEGIN
			SET	consulta = 
			CONCAT(consulta, ' AND fechped BETWEEN CAST(',"'", p_fechain,"'",' AS DATE)	AND	CAST(',"'",p_fechafi,"'",' AS DATE)');
		END;
	END	IF;
	
	IF p_banco <> "0" THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND banco	= ', "'", p_banco, "'");
		END;
	END	IF;
	IF p_categoria > 0 THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND codcate =	', p_categoria
			);
		END;
	END	IF;


SET	@query = consulta;

PREPARE	strsql FROM	@query;
EXECUTE	strsql;

END
$$
DELIMITER ;


-- ----------------------------------------------------------------------

/*
DROP PROCEDURE IF EXISTS pa_dbcanasteria_vt_mostrar_ventasxbancos;
DELIMITER $$
CREATE DEFINER=`rolivos`@`%` PROCEDURE `pa_dbcanasteria_vt_mostrar_ventasxbancos`(IN `p_fechain` CHAR(10),IN `p_fechafi` CHAR(10),
 IN	`p_banco` VARCHAR(30))
BEGIN
DECLARE	consulta VARCHAR(2000);

SET	consulta = 
	'select	
		IFNULL(vi.descbanco,"")AS descbanco, IFNULL(vi.tarjeta,"")AS tarjeta,IFNULL(SUM(impdscto),0) AS	impdscto,
		IFNULL(SUM(cantive),0) AS cantive,(SELECT IFNULL(SUM(impvent),0) FROM tblventas	v WHERE	v.codvent =	codvent) AS	impvent
	from v_reporte_Ventas vi 
	where 1	= 1';

	IF p_fechain <>	'' THEN
		BEGIN
			SET	consulta = 
			CONCAT(consulta, ' AND fechped BETWEEN CAST(',"'", p_fechain,"'", 
			' AS DATE) AND CAST(',"'",p_fechafi,"'",' AS DATE)');
		END;
	END	IF;
	
	IF p_banco <> "0" THEN
		BEGIN
			SET	consulta = CONCAT(consulta,	' AND banco	= ', "'", p_banco, "'");
		END;
	END	IF;
	
	SET	consulta = CONCAT( consulta, ' GROUP BY	banco');


SET	@query = consulta;

PREPARE	strsql FROM	@query;
EXECUTE	strsql;

END$$
DELIMITER ;
*/




DROP PROCEDURE IF EXISTS pa_dbcanasteria_vt_mostrar_ventasxbancos;
DELIMITER $$
CREATE PROCEDURE pa_dbcanasteria_vt_mostrar_ventasxbancos( p_fechain CHAR(10), p_fechafi CHAR(10), p_banco VARCHAR(30))
BEGIN
	DECLARE	consulta VARCHAR(2000);
	DECLARE	sql_filtros VARCHAR(1000);
	
	SET sql_filtros = '';
	IF p_fechain <> '' AND p_fechafi <> '' THEN
		SET sql_filtros = CONCAT(sql_filtros, ' AND fechped BETWEEN CAST(',"'", p_fechain,"'",' AS DATE) AND CAST(',"'",p_fechafi,"'",' AS DATE)');
	END	IF;
	
	IF p_banco <> "0" THEN
		SET	sql_filtros = CONCAT(sql_filtros, ' AND codbanc = ', "'", p_banco, "'");
	END	IF;
	
	SET consulta = ' select (SELECT IFNULL(b.descripcion,"") FROM tblbanco b WHERE b.codbanco = v.codbanc AND b.marcabaja= 0) AS descbanco, IFNULL(v.tiptarj,"") AS tarjeta,';
	
	SET consulta = CONCAT(consulta, ' (SELECT IFNULL(SUM(dv.importe),0) FROM tbldescuentoventa dv WHERE dv.codvent IN (SELECT codvent FROM tblventas WHERE marBaja = 0 ');
    
    SET consulta = CONCAT(consulta, sql_filtros);
    
    SET consulta = CONCAT(consulta, ' AND codbanc = v.codbanc) AND dv.marBaja= 0) AS impdscto,');
	
	SET consulta = CONCAT(consulta, ' COUNT(codvent) AS cantive, IFNULL(SUM(impvent),0) AS impvent FROM tblventas v where v.marBaja = 0 ');
	
	SET consulta = CONCAT(consulta, sql_filtros);
	
	SET consulta = CONCAT(consulta, ' GROUP BY v.codbanc, v.tiptarj');
	
    -- SELECT consulta;
    
	SET	@query = consulta;

	PREPARE	strsql FROM	@query;
	EXECUTE	strsql;

END$$
DELIMITER ;

-- ---------------------- CARGAR VENTAS	-----------------

DROP PROCEDURE IF EXISTS `pa_dbcanasteria_vt_cargarventas`;
DELIMITER $$
CREATE PROCEDURE `pa_dbcanasteria_vt_cargarventas`(
	p_codped INT, 
	p_fechped VARCHAR(20), 
	p_impped DECIMAL(14,2),
	p_codbin VARCHAR(100), 
	p_xmlpro TEXT,
	p_ip VARCHAR(70)
)
BEGIN

DECLARE	l_item			INT;	
DECLARE	l_articulo		VARCHAR(300);
DECLARE	l_descripcion	VARCHAR(700);
DECLARE	l_categoria		INT;
DECLARE	l_cantventa		INT;
DECLARE	l_impventa		DECIMAL(14,2);
DECLARE	l_descuento		INT;
DECLARE	l_impdescuento	DECIMAL(14,2);
DECLARE	l_ntra			INT;
DECLARE	l_banco			VARCHAR(100);
DECLARE	l_tarjeta		VARCHAR(300);
DECLARE	cnt	INT;
DECLARE	ptr	int	DEFAULT	0;
DECLARE	rowPtr varchar(100);
DECLARE	mensaje	varchar(300);
DECLARE	codigo INT;
DECLARE	l_codbin VARCHAR(100) DEFAULT "";
	
SET	l_item			  =	0;	  
SET	l_articulo		  =	"";
SET	l_descripcion	  =	"";
SET	l_categoria		  =	0;
SET	l_cantventa		  =	0;
SET	l_impventa		  =	0;
SET	l_descuento		  =	0;
SET	l_impdescuento	  =	0;
SET	l_ntra			  =	0;
SET	mensaje			  =	"";	 
SET	codigo			  =	0;
SET	l_banco			  =	"";
SET	l_tarjeta		  =	"";

	SELECT 
		banco,
		(SELECT	descripcion	FROM tblconcepto WHERE codConcepto = 2 AND correlativo = tarjeta AND correlativo > 0),
		codbin 
	INTO l_banco, l_tarjeta, l_codbin
	FROM  tblcodbin
	WHERE marcabaja	= 0	AND	sha2(codbin,224) = p_codbin;

	START TRANSACTION;
	BEGIN
		DELETE FROM	tblventas WHERE	codvent	= p_codped AND fechped = cast(p_fechped	AS date);
		
		INSERT INTO	tblventas(codvent,fechped,impvent,codibin,codbanc,tiptarj,marbaja,fecProc, horaPro,	usuario,ip)	
		VALUES(p_codped,cast(p_fechped AS date)	,p_impped,l_codbin,l_banco,	l_tarjeta,0,(select	CURDATE()),(select curTime()),(select current_user()),p_ip);
	END;
	
	BEGIN
		DELETE FROM	tbldetventa	WHERE codvent =	p_codped;
		DELETE FROM	tbldescuentoventa WHERE	codvent	= p_codped;
	END;
	
	SET	cnt	= (extractValue(p_xmlpro, 'count(/detalleproducto/item)'));
	SET	ptr	= 0;
	WHILE ptr <	cnt	do
		SET	l_item			  =	0;	  
		SET	l_articulo		  =	"";
		SET	l_descripcion	  =	"";
		SET	l_categoria		  =	0;
		SET	l_cantventa		  =	0;
		SET	l_impventa		  =	0;
		SET	l_descuento		  =	0;
		SET	l_impdescuento	  =	0;
		SET	l_ntra = 0;
		SET	ptr	= ptr +	1;
		
		SET	rowPtr = concat('/detalleproducto[', ptr, ']');
   
		-- SET	l_item		   = extractValue(p_xmlpro,	concat(rowPtr, '/item'));
		SET	l_item		   = ptr;
		SET	l_articulo	   = extractValue(p_xmlpro,	concat(rowPtr, '/articulo'));
		SET	l_descripcion  = extractValue(p_xmlpro,	concat(rowPtr, '/descripcion'));
		SET	l_categoria	   = extractValue(p_xmlpro,	concat(rowPtr, '/categoria'));
		SET	l_cantventa	   = extractValue(p_xmlpro,	concat(rowPtr, '/cantventa'));
		SET	l_impventa	   = extractValue(p_xmlpro,	concat(rowPtr, '/impventa'));
		SET	l_descuento	   = extractValue(p_xmlpro,	concat(rowPtr, '/descuento'));
		SET	l_impdescuento = extractValue(p_xmlpro,	concat(rowPtr, '/impdescuento'));
		
		BEGIN
			INSERT INTO	tbldetventa	(codvent,numitem,codarti,desarti,codcate,cantive,importe,marBaja,fecProc,horaPro,usuario,ip) 
			VALUES (p_codped,l_item,l_articulo,l_descripcion,l_categoria,l_cantventa,l_impventa,0,(select CURDATE()),(select curTime()),(select	current_user()),p_ip);
			
			INSERT INTO	tbldescuentoventa(codvent,coddesc,numitem,codarti,importe,marBaja,fecProc,horaPro,usuario,ip)
			VALUES(p_codped,l_descuento,l_item,l_articulo,l_impdescuento,0,(select CURDATE()),(select curTime()),(select current_user()),p_ip);
			SET	l_ntra = LAST_INSERT_ID();
		END;

	END	WHILE; 
	COMMIT;
	
	IF l_ntra >	0 THEN 
		SET	mensaje	= "REGISTRO	EXITOSO";
		SET	codigo = 0;	
	ELSE
		SET	mensaje	= "ERROR AL	REGISTRAR VENTAS";
		SET	codigo = 1;	
	END	IF;
	
	SELECT mensaje,	codigo;

END$$
DELIMITER ;