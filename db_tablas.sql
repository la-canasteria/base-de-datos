use db_canasteria
;
DROP DATABASE db_canasteria
;
CREATE DATABASE db_canasteria
;
use db_canasteria
;
-- -----------------------------------------------------
-- Table `dbo.tblBanco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tblBanco` (
  `codBanco` varchar(10) NOT NULL PRIMARY KEY,
  `abreviatura` varchar(20) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `marcaBaja` tinyint(1) NOT NULL DEFAULT '0',
  `fechaProceso` date NOT NULL,
  `horaProceso` time NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `mac` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `dbo.tblCodBIN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tblCodBIN` (
  `ntraParametrizador` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `codBIN` varchar(20) NOT NULL,
  `marca` int(11) NOT NULL,
  `tarjeta` tinyint(1) NOT NULL,
  `banco` varchar(10) NOT NULL,
  `marcaBaja` tinyint(1) NOT NULL DEFAULT '0',
  `fechaProceso` date NOT NULL,
  `horaProceso` time NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `mac` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `tblConcepto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tblConcepto` (
  `ntraConcepto` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `codConcepto` int(11) NOT NULL,
  `correlativo` int(11) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `marcaBaja` tinyint(1) NOT NULL DEFAULT '0',
  `fechaProceso` date NOT NULL,
  `horaProceso` time NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `mac` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `dbo.tblDescuentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tblDescuentos` (
  `ntraDescuento` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `descripcion` varchar(250) DEFAULT NULL,
  `fechaInicial` date NOT NULL,
  `fechaFin` date NOT NULL,
  `horaInicial` time NOT NULL,
  `horaFin` time NOT NULL,
  `estado` int(1) NOT NULL, -- SELECT * FROM tblconcepto WHERE codConcepto = 3
  `tipoDescuento` tinyint(1) NOT NULL DEFAULT '1', -- SELECT * FROM tblconcepto WHERE codConcepto = 6
  `marcaBaja` tinyint(1) NOT NULL DEFAULT '0',
  `fechaProceso` date NOT NULL,
  `horaProceso` time NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `mac` varchar(20) DEFAULT NULL,
  `codSucursal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `dbo.tblDetalleDescuentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tblDetalleDescuentos` (
  `ntraDescuento` int(11) NOT NULL,
  `flag` int(11) NOT NULL, -- SELECT * FROM tblconcepto WHERE codConcepto = 4
  `valorInicial` varchar(200) NOT NULL,
  `valorFinal` varchar(200) NOT NULL,
  `detalle` tinyint(1) NOT NULL DEFAULT '0',
  `estadoDetalle` tinyint(1) DEFAULT '1',
  `marcaBaja` tinyint(1) NOT NULL DEFAULT '0',
  `fechaProceso` date NOT NULL,
  `horaProceso` time NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `mac` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `dbo.tblPerfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tblPerfil` (
  `codigo` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `marcaBaja` tinyint(1) NOT NULL DEFAULT '0',
  `fechaProceso` date NOT NULL,
  `horaProceso` time NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `mac` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `dbo.tblPersona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tblPersona` (
  `codPersona` int(11) NOT NULL PRIMARY KEY,
  `tipoPersona` tinyint(1) NOT NULL,
  `tipoDocumento` tinyint(1) DEFAULT NULL,
  `numeroDocumento` varchar(15) DEFAULT NULL,
  `ruc` varchar(15) DEFAULT NULL,
  `razonSocial` varchar(150) DEFAULT NULL,
  `nombres` varchar(30) DEFAULT NULL,
  `apellidoPaterno` varchar(20) DEFAULT NULL,
  `apellidoMaterno` varchar(20) DEFAULT NULL,
  `direccion` varchar(100) NOT NULL,
  `correo` varchar(60) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `celular` char(9) DEFAULT NULL,
  `codUbigeo` char(6) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `marcaBaja` tinyint(1) NOT NULL DEFAULT '0',
  `fechaProceso` date NOT NULL,
  `horaProceso` time NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `mac` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `dbo.tblUsuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tblUsuario` (
  `ntraUsuario` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `users` varchar(50) NOT NULL,
  `estado` int(11) NOT NULL,
  `password` varbinary(8000) NOT NULL,
  `codSucursal` int(11) DEFAULT NULL,
  `marcaBaja` tinyint(1) NOT NULL DEFAULT '0',
  `fechaProceso` date NOT NULL,
  `horaProceso` time NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `mac` varchar(20) DEFAULT NULL,
  `codPersona` int(11) NOT NULL,
  `codPerfil` int(11) NOT NULL,
  FOREIGN KEY (`codPersona`) REFERENCES `tblpersona` (`codPersona`),
  FOREIGN KEY (`codPerfil`) REFERENCES `tblperfil` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `dbo.tbllogws`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tbllogws` (
  `codiden` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `direurl` VARCHAR(200) NOT NULL,
  `webserv` VARCHAR(200) NOT NULL,
  `tipolog` SMALLINT NOT NULL,
  `flagerr` SMALLINT NOT NULL DEFAULT 0,
  `origerr` SMALLINT NULL,
  `logcerr` VARCHAR(400) NULL,
  `descerr` TEXT NULL,
  `datentr` TEXT NULL,
  `datsali` TEXT NULL,
  `marBaja` SMALLINT NOT NULL DEFAULT 0,
  `fecProc` DATE NOT NULL,
  `horaPro` TIME NOT NULL,
  `usuario` VARCHAR(20) NOT NULL,
  `ip` VARCHAR(20) NULL,
  `mac` VARCHAR(20) NULL)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `dbo.tblCategoria`
-- -----------------------------------------------------

CREATE TABLE `tblCategoria` (
  `codCategoria` int NOT NULL,
  `descCategoria` varchar(100) NOT NULL,
  `marcaBaja` tinyint(1) NOT NULL DEFAULT '0',
  `fechaProceso` date NOT NULL,
  `horaProceso` time NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `mac` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`codCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

-- -----------------------------------------------------
-- Table `dbo.tblParametros`
-- -----------------------------------------------------

CREATE TABLE `tblParametros` (
  prefijo int NOT NULL,
  correlativo SMALLINT NULL,
  estado SMALLINT NULL,
  tipo SMALLINT NULL,
  smallint1 SMALLINT NULL,
  smallint2 SMALLINT NULL,
  entero1 INT NULL,
  entero2 INT NULL,
  float1 FLOAT NULL,
  float2 FLOAT NULL,
  fecha1 DATE NULL,
  fecha2 DATE NULL,
  string1 VARCHAR(255),
  string2 VARCHAR(255)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

-- -----------------------------------------------------
-- Table `dbo.tblerroresweb`
-- -----------------------------------------------------

CREATE TABLE `tblerroresweb` (
  ntraError INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  sistema VARCHAR(255) NULL,
  modulo VARCHAR(255) NULL,
  opcion VARCHAR(255) NULL,
  tipo SMALLINT NULL,
  descripcion TEXT NULL,
  fechaProceso date NOT NULL,
  horaProceso time NOT NULL,
  usuario varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

CREATE TABLE IF NOT EXISTS `tblventas` (
  `codiden` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `codvent` VARCHAR(200) NOT NULL,                #codigo de pedido
  `fechped` DATE NOT NULL,												#fecha de pedido
  `impvent` DECIMAL(14,2) NOT NULL,               #importe total de venta
  `codiBin` VARCHAR(30) NOT NULL,                 #Codigo BIN
  `codbanc` VARCHAR(400)NOT NULL,                 #codigo de Banco (ejempl: BC003)
  `tiptarj` VARCHAR(400)NOT NULL,                 #Tipo de tarjeta(ejempl: CREDITO - TARJETA BLACK BBVA)
  `marBaja` SMALLINT NOT NULL DEFAULT 0,
  `fecProc` DATE NOT NULL,
  `horaPro` TIME NOT NULL,
  `usuario` VARCHAR(20) NOT NULL,
  `ip` VARCHAR(20) NULL,
  `mac` VARCHAR(20) NULL)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `tbldetventa` (
  `codiden` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `codvent` VARCHAR(200) NOT NULL,                #codigo de pedido
  `numitem` SMALLINT NOT NULL,												#numero item de pedido 
  `codarti` VARCHAR(100) NOT NULL,                #codigo de articulo
  `desarti` VARCHAR(500) NOT NULL,                #descripcion articulo
  `codcate` INT NOT NULL,                         #codigo de categoria
  `cantive` SMALLINT NOT NULL,                    #cantidad vendida
  `importe` DECIMAL(14,2) NOT NULL,               #Importe venta
  `marBaja` SMALLINT NOT NULL DEFAULT 0,
  `fecProc` DATE NOT NULL,
  `horaPro` TIME NOT NULL,
  `usuario` VARCHAR(20) NOT NULL,
  `ip` VARCHAR(20) NULL,
  `mac` VARCHAR(20) NULL)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `tbldescuentoventa` (
  `codiden` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `codvent` INT NOT NULL,                					#codigo de pedido
  `coddesc` INT NOT NULL,                					#codigo de descuento
  `numitem` SMALLINT NOT NULL,                    #Item
  `codarti` VARCHAR(100) NOT NULL,                #codigo de articulo
  `importe` DECIMAL(14,2)NOT NULL,               #Importe descuento
  `marBaja` SMALLINT NOT NULL DEFAULT 0,
  `fecProc` DATE NOT NULL,
  `horaPro` TIME NOT NULL,
  `usuario` VARCHAR(20) NOT NULL,
  `ip` VARCHAR(20) NULL,
  `mac` VARCHAR(20) NULL)
ENGINE = InnoDB;